# Java Introduction

## Why Java?

Java is a language that is based at C++. Java takes care of memory management and 
owns automatic garbage collector which collects the unused objects automatically. Resuming the whys:
* Simple language
* Platform-independent
* OOP
* Robust
* Multithreaded

## What is a JAR file?
JAR files are Java Archive based on pkzip file format and corresponde to the deployed Java files.  
They can contain: 
* Classes
* Icons
* etc

__NOTE:__ Adding JAR files to your classpath will allow you to use its classes in your Java app.

### Executable JAR
Allows the user to run the application without the need to specify the class file that should be started. This can be achieved via a **MANIFEST.MF**.

[Original example source code](https://www.vogella.com/tutorials/JavaLibraries/article.html#jarfiles_executable)
```MANIFEST.MF
    Manifest-Version: 1.0
    Main-Class: com.vogella.example.MyApp
    Class-Path:. lib/jcommon-1.0.6.jar lib/itext-1.4.6.jar
    
```
__NOTE:__ Empty line required at the end of **MANIFEST.MF**

```bash
    jar -cvmf MANIFEST.MF app1.jar *.class
```
> -c or --create
>> To create the archive
> 
> -v or --verbose
>> To have a generous and more informative output
> 
> -m or --manifest=FILE
>> To include the MANIFESt information we desire for the project
> 
> -f or --file=FILE
>> Creates the archive file name. When omitted uses what is either at stdin or stdout
> 
> app1.jar
>> As the name of the JAR file we want to give
> 
> *.class
>> All the classes we want the JAR file to target. In this case, any file that ends in **.class** is being targetted for
> the folder where the command's being executed
> 

### Useful JAR commands

__Create a JAR file__
```bash
    jar cf jar-filename input-file(s)
```

__List JAR contents__
```bash
    jar tf jar-filename
```

__Extract contents of a JAR__
```bash
    jar xf jar-filename
```

__Compile__
```bash
    javac -d <dest folder for .class files> -sourcepath <src folder1;src folder2...> <path to class with main function>
```

__Run a JAR app__
```bash
    java -jar jar-file
```


## Java SDKs
Software Development Kits **(aka SDKs)** can also be mentioned as **JDK** (mentioned to be one and the same).  

Confusion started to arise whenOracle changed their support model for Java and started releasing new bigger 
versions every 6 months.

There are a few points to consider when referring to JDK:

* Two JDK builds distributed:
    * Oracle JDK
    * Oracle OpenJDK
* Oracle JDK is free for development but its paid if used at production
* Oracle's OpenJDK is free for any environment;

Raisin makes use of a [OpenJDK](https://openjdk.java.net/projects/jdk/)

### SDKMAN
This is a tool to help download and select Java JDK easily. Java official can restrict which JDK allows access to at 
their official page, as versions progress, so SDKMAN really hits home when helping out locating them.

It allows the use of parallel version on unix systems for multiple SDKs.

__Check available versions__
```bash
    sdk install java
```

## Makefile
Ant is similar to makefile and introduced as one of the many production tools ccreated by java devs.  
In contrast with the makefile, Ant is types with a __xml format__.


## Java at Raisin

The Java version applied to most of the Raisin projects is JDK 8. 

I took the liberty to look into installing both JDK8 and 17 into my local. 
Then I can switch around each as needed and explore more about them.

