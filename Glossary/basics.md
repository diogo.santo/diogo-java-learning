#Java Basics

## Packages
> Think of it as folder structures or even as Bundles for Symfony. 
> They are a good way to avoid naming mismatch and correctly structure code 

## Visibility

> Public
>> Accessible from anywhere by anyone
> 
> Protected
> 
>> Accessible only by instantiation of class
> 
> Default
> 
>> Acessible only inside the package
> 
> Private (Most restrictive)
> 
>> Accessible only inside the class

Code Example:
'''java
    int x;
    protected int y;
'''

Both above vars are visible inside the package.  
**int x;** is of **default** visibility.

## Data Structures

### Arrays

```java
    int[] myArray;
    myArray = new int[4];
```

**Arrays - values initialization**
```java
    myArray = new int[] {0,4,3,7,10};
```

### For Loops

**old way**
```java
    for(int i = 0; i < myArray.lenght; i++) {...}
```

**new way**
```java
    for(int number : myArray){...}
```

### Constants

* Variable that can't be change once a value is assigned;
* Usually static;
* Upper case;

**Constant vs Final**

__Final__ can't be changed once instantiated and value assigned;  
__Constant__ is defined at compile-time and the object is therefore permanently frozen;







