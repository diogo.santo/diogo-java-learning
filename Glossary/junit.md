# About JUnit

JUnit allows us to build unit testing for our java code.  
For such, we need to download the JUnit packages and start writting our tests.  

Just like PHPUnit, JUnit tests do not need to be kept along side the Java files and, 
similarly, provide methods to help us tests the code.

## Console Run

[Documentation](https://junit.org/junit5/docs/current/user-guide/#running-tests-console-launcher)



## @Test

This is a tag that needs adding at the top of functions we want to run through the JUnit test machine.  
If not present, JUnit will just skip it.

```java
public class AssertTest {
    ...
    @Test
    public void myTest() {
        assertEquals(2, 2); //I know... I know
    }
    ...
}
```