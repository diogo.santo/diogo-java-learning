![Java composition](./assets/JDK.png)

### JVM
[Source](https://www.geeksforgeeks.org/differences-jdk-jre-jvm/)
> Java Virtual Machine
>
> A virtual machine that allows a computer to run Java programs or other languages that compile into Java bytecode
>> Note: "Whenever you write a java command on the command prompt to run a java class, an instance of JVM is created"

### JRE or Java RTE
[Source](https://www.geeksforgeeks.org/differences-jdk-jre-jvm/)
> **Java Runtime Environment**
> 
> Provides the minimum requirements to run Java applications.
>> Consists of:
>> * JVM
>> * Core classes
>> * Supporting files

### JDK
[Source](https://www.geeksforgeeks.org/differences-jdk-jre-jvm/)
> **Java Development Kit**
>
> Used to develop Java applications
>> Consists of:
>> * Development Tools
>> * JRE