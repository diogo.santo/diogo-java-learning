# Diogo Java Learning Project

This is a small repo to contain most of the information about Java that I have been finding and to store my progress in 
the language itself. This allows to keep track of the learning as well as easily ask for support with regard to certain 
issues I might find or to standards applied by Raisin.

## Index
1. [Glossary](/Glossary/glossary.md)
2. [Introduction](/Glossary/introduction.md)
3. [Basic](/Glossary/basics.md)
3. [JUnit](/Glossary/junit.md)

## Tasks
> Task 1
>> Without any IDE and build tools create a java library packaged as JAR which provides a utility method that does 
>> quicksort of an array of strings. Write unit tests with JUnit library for this function and learn how to run these 
>> tests. (edited)

> Task 2
>> To library developed in Task 1 add another implementation of sorting function that uses Java SDK to do a quicksort.
>> Find a library for benchmarking and run benchmarks for your custom solution and another one that uses SDK and compare
>> result

> Task 3
>> Without any IDE and build tools create a CLI application that takes list of strings as arguments and sorts them using
>> library you developed in Task 1. Make sure you don't copy-paste code of the library but plug-in it as a dependency