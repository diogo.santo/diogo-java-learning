import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.*;

public class Window {

    public static void main(String args[]) {
        JFrame window = new JFrame();

        window.setTitle("Raisin Window");
        window.setSize(800, 600);
        window.setVisible(true);

        JLabel label = new JLabel();
        label.setText("A label goes here");
        label.setForeground(Color.black);
        label.setVisible(true);

        window.add(label);
    }

}
