## Compiling
```bash
    cd src/
    javac -d ../out/production/ ./*.java
```

```bash
    cd ../
    java -cp out/production Main
```

## Compiling for tests
```bash
    javac -d ../out/tests -cp ../libs/junit-4.13.2.jar:../out/production tests/sort/*.java
```

```bash
    java -cp out/tests:libs/junit-4.13.2.jar:libs/hamcrest-core-1.3.jar:out/production/ org.junit.runner.JUnitCore tests.sort.QuickSortTest
```