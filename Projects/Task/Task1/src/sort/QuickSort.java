package sort;

public class QuickSort <T> extends Sort {

    public QuickSort(Comparable<T>[] toSort) {
        super(toSort);
    }

    public QuickSort sort() {
        this.partition(this.toSort);
        return this;
    }

    //Overload partition method
    private void partition(Comparable <T>[] partition) {
        partition(partition, 0, (this.toSort.length -1));
    }

    /**
     * Move all elements lower than pivot to its left and all higher than pivot to its right
     */
    private void partition( Comparable <T>[] partition, int leftIndex, int rightIndex ) {
        if (leftIndex >= rightIndex)
            return;

        Comparable <T> pivot = partition[rightIndex];
        int leftPointer = leftIndex;
        int rightPointer = rightIndex;

        while (leftPointer < rightPointer) {
            leftPointer = this.moveLeftPointer(leftPointer, rightPointer, pivot);
            rightPointer = this.moveRightPointer(leftPointer, rightPointer, pivot);

            // Since we loop the pointer previously, when reaching here we are sure to be holding the correct values to
            // swap at the array
            super.swap(partition, leftPointer, rightPointer);
        }

        // Right index stores the pivot value pointer.
        // We need to replace pivot to center it
        super.swap(partition, leftPointer, rightIndex);

        this.partition(partition, leftIndex, (leftPointer - 1) );
        this.partition(partition, (leftPointer + 1), rightIndex );
    }

    public int moveLeftPointer(int leftPointer, int rightPointer, Comparable <T> pivot) {
        while ( (0 >= (this.toSort[leftPointer]).compareTo(pivot)) && leftPointer < rightPointer ) {
            leftPointer++;
        }

        return  leftPointer;
    }

    public int moveRightPointer(int leftPointer, int rightPointer, Comparable <T> pivot) {
        while ( (0 <= this.toSort[rightPointer].compareTo(pivot)) && leftPointer < rightPointer ) {
            rightPointer--;
        }

        return  rightPointer;
    }
}