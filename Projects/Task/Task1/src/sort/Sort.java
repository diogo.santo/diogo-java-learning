package sort;

public abstract class Sort <T extends Comparable<T>> {

    protected Comparable <T>[] toSort;

    public Sort(T[] toSort) {
        this.toSort = toSort;
    }

    public abstract Sort sort();

    protected void swap(Comparable <T>[] target, int index1, int index2)
    {
        Comparable <T> temp = target[index1];
        target[index1] = target[index2];
        target[index2] = temp;
    }

    public Comparable <T>[] getToSort() {
        return this.toSort;
    }
}