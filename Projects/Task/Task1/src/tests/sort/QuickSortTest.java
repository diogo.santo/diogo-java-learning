package tests.sort;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import sort.QuickSort;

import java.util.Arrays;
import java.util.Collection;
import java.lang.Comparable;

@RunWith(Parameterized.class)
public class QuickSortTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new Integer[] {1,6,2,7,9,4}, new Integer[] {1, 2, 4, 6, 7, 9}},
                {new Integer[] {1,60,35,71,904,4005}, new Integer[] {1, 35, 60, 71, 904, 4005}},
                {new String [] {"A", "Z", "B", "H", "F", "C"}, new String[] {"A", "B", "C", "F", "H", "Z"}},
                {new String [] {"C", "X", "B", "A", "J", "L"}, new String[] {"A", "B", "C", "J", "L", "X"}},
                {
                    new String[] {"R","G","D","I","V","M", "Y", "R", "Z", "L", "F", "B", "S", "D", "B", "G", "W", "T", "B", "J"},
                    new String[] {"B", "B", "B", "D", "D", "F", "G", "G", "I", "J", "L", "M", "R", "R", "S", "T", "V", "W", "Y", "Z"}
                },
        });
    }

    private final QuickSort<?> quickSort;
    private final Comparable[] expected;

    public QuickSortTest(Comparable[] test, Comparable[] expected) {
        this.expected = expected;
        this.quickSort = new QuickSort<>(test);
    }

    @Test
    public void checkQuickSortIsSortingArrays() {
        Assert.assertArrayEquals(quickSort.sort().getToSort(), expected);
    }

}
