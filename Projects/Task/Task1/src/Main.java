import sort.QuickSort;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] test = new String[] {"A", "Z", "B", "H", "F", "C"};
        System.out.println(Arrays.toString(test));

        QuickSort<String> sortObj = new QuickSort<>(test);
        sortObj.sort();
        System.out.println(Arrays.toString(sortObj.getToSort()));

        Integer[] testInt = new Integer[] {2, 6, 4, 9, 1};
        System.out.println(Arrays.toString(testInt));

        QuickSort<Integer> sortObjInt = new QuickSort<>(testInt);
        sortObjInt.sort();
        System.out.println(Arrays.toString(sortObjInt.getToSort()));
    }
}